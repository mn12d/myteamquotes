> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Header 1

## Header 2

Now is the time for all good men to come to
the aid of their country. This is just a
regular paragraph.

The quick brown fox jumped over the lazy
dog's back.

### Header 3

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> ## This is an H2 in a blockquote

1. LIS 4381 Fall2015 a1
2. Maury Neipris
3. Assignment requirements
[StationLocations repo](https://bitbucket.org/mn12d/bitbucketstationlocations "StationLocations")
[teamquotes repo](https://bitbucket.org/mn12d/myteamquotes "teamquotes")
[lis4381_fall15_a1](https://bitbucket.org/mn12d/lis4381_fall15_a1 "lis4381_fall15_a1")

![screenshot](https://bitbucket.org/mn12d/lis4381_fall15_a1/raw/master/ampps.png)

4. Git commands and short description

git init- create an empty git repo or reinitialize an existing one

git status- show the working tree status  

git add- add file contents to the index  

git commit- record changes to the repo  

git push- update remote references along with associated objects  

git pull- fetch from and integrate with another repository or a local branch  

git config- get and set repository or global options



5. Link to your Web site

 [My website](http://www.mauryneipris.com/LIS4381/a1/ "My Site")
